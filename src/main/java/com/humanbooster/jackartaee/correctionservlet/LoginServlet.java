package com.humanbooster.jackartaee.correctionservlet;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name="login", value = "/login")
public class LoginServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");

        PrintWriter out = resp.getWriter();
        out.println("<html><body>");
        out.println("<h1>Formulaire de connexion</h1>");
        out.println("<form method='post'>");

        out.println("<label>Username</label>");
        out.println("<input type='text' name='username' placeholder='Username'>");

        out.println("<label>Password</label>");
        out.println("<input type='password' name='password' placeholder='Password'>");

        out.println("<input type='submit'/></form>");
        out.println("</body></html>");
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        if(username.equals("admin") && password.equals("admin")){
            resp.setContentType("text/html");

            PrintWriter out = resp.getWriter();
            out.println("<html><body>");
            out.println("<h1>Bienvenue Admin !</h1>");
        } else {
            resp.setContentType("text/html");
            PrintWriter out = resp.getWriter();
            out.println("<html><body>");
            out.println("<h1>Formulaire de connexion</h1>");
            out.println("<form method='post'>");
            out.println("<label>Username</label>");
            out.println("<input type='text' name='username' placeholder='Username'>");
            out.println("<label>Password</label>");
            out.println("<input type='password' name='password' placeholder='Password'>");
            out.println("<div class='error'>Les identifiants ne sont pas valides</div>");
            out.println("<input type='submit'/></form>");
            out.println("</body></html>");
        }
    }
}
