package com.humanbooster.jackartaee.correctionservlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name="register", value = "/register")
public class RegisterServlet extends HttpServlet {
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><body>");
        out.println("<h1>Formulaire d'enregistrement</h1>");
        out.println("<form method='post'>");

        out.println("<label>Username</label>");
        out.println("<input type='text' name='username' placeholder='Username'>");

        out.println("<label>Password</label>");
        out.println("<input type='password' name='password' placeholder='Password'>");

        out.println("<label>Confirmation du mot de passe</label>");
        out.println("<input type='password' name='confirm' placeholder='Confirmez votre mot de passe'>");

        out.println("<input type='submit'/></form>");
        out.println("</body></html>");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String confirmPassword = request.getParameter("confirm");

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();

        if(!password.equals(confirmPassword)){
            out.println("<html><body>");
            out.println("<h1>Formulaire d'enregistrement</h1>");
            out.println("<form method='post'>");

            out.println("<label>Username</label>");
            out.println("<input type='text' name='username' placeholder='Username'>");

            out.println("<label>Password</label>");
            out.println("<input type='password' name='password' placeholder='Password'>");

            out.println("<label>Confirmation du mot de passe</label>");
            out.println("<input type='password' name='confirm' placeholder='Confirmez votre mot de passe'>");

            out.println("<div class='error'>Les mots de passes ne sont pas identiques");

            out.println("<input type='submit'/></form>");
            out.println("</body></html>");
        } else if (username.isEmpty()) {

            out.println("<html><body>");
            out.println("<h1>Formulaire d'enregistrement</h1>");
            out.println("<form method='post'>");

            out.println("<label>Username</label>");
            out.println("<input type='text' name='username' placeholder='Username'>");
            out.println("<div class='error'>Veuillez renseigner votre username</div>");
            out.println("<label>Password</label>");
            out.println("<input type='password' name='password' placeholder='Password'>");

            out.println("<label>Confirmation du mot de passe</label>");
            out.println("<input type='password' name='confirm' placeholder='Confirmez votre mot de passe'>");

            out.println("<div class='error'>Les mots de passes ne sont pas identiques");

            out.println("<input type='submit'/></form>");
            out.println("</body></html>");
        } else {
            out.println("<html><body>");
            out.println("<h1>Bienvenue "+username+"</h1>");
            out.println("</body></html>");
        }
        System.out.println("Hello");
    }

}
